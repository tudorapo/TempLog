CREATE OR REPLACE FUNCTION toplist()
RETURNS void AS $$
DECLARE 
    sor record;
BEGIN
    DROP TABLE IF EXISTS toplist;
    CREATE TABLE toplist (
        name TEXT,
        datum TEXT,
        value TEXT,
        unit TEXT,
        temp DOUBLE PRECISION,
        type TEXT,
        what TEXT,
        splice TEXT,
        sortby INT
    );
    FOR sor IN (
        SELECT * from times
    ) LOOP
        INSERT INTO toplist SELECT 
            records.name, 
            to_char(to_timestamp(records.epoch), 'YYYY TMMonth FMDDth, FMHH24:MI') AS datum, 
            to_char(records.temp, '9999D9') AS value, 
            units.unit,
            records.temp,
            records.type,
            records.what,
            sor.name AS splice,
            sor.delta as sortby
        FROM ((
                SELECT DISTINCT ON (name, type)
                    name,
                    epoch,
                    temp,
                    type,
                    'max' AS what 
                FROM log 
                WHERE 
                    type != 'snd' AND 
                    type != 'sdb' AND 
                    type != 'lux' AND 
                    type != 'ilux' AND 
                    epoch > cast(extract(epoch from current_timestamp) as integer) - sor.delta
                ORDER BY name ASC, type, temp DESC
            ) UNION (
                SELECT DISTINCT ON (name, type) 
                    name,
                    epoch,
                    temp,
                    type,
                    'min' AS what 
                FROM log 
                WHERE 
                    type != 'snd' AND 
                    type != 'sdb' AND 
                    type != 'lux' AND 
                    type != 'ilux' AND 
                    epoch > cast(extract(epoch from current_timestamp) as integer) - sor.delta
                ORDER BY name ASC, type, temp ASC
        )) AS records, units 
        WHERE units.type = records.type 
        ORDER BY records.type, records.name ASC, records.temp ASC;
    END LOOP;
END;
$$ LANGUAGE plpgsql VOLATILE;
