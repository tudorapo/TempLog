#!/usr/bin/python3
import sqlite3
import datetime
from subprocess import call

resolution = 15 # number of steps when drawing the shaded filledcurves
months = 16 # number of months to count the xtics, must be more than the next value
daysback = 390 # how much time should be shown in the diagram, must be smaller than the previous value

conn = sqlite3.connect('temp.db')
curs = conn.cursor()

curs.execute('SELECT DISTINCT name FROM log WHERE type like "temp%";');
names = curs.fetchall()

#Writing two files, as gnuplot is unable to loop for the stdin.

f = open('/tmp/yearfog.txt','w')

for name in names:
    curs.execute('''
        SELECT 
            strftime("%Y-%m-%d",epoch,"unixepoch","localtime","-7 days","weekday 1") AS monday, 
            avg(temp) AS avg
        FROM 
            log
        WHERE
            name = ? AND type like "temp%"
        GROUP BY 
            name,
            monday
        ORDER BY
            epoch;''',name)
    avgs = curs.fetchall()
    curs.execute('''
        SELECT 
            strftime("%Y-%m-%d",epoch,"unixepoch","localtime","-7 days","weekday 1") AS monday, 
            temp
        FROM log 
        WHERE
            name = ? AND type like "temp%"
        ORDER BY  
            monday,
            temp;''',name)
    data = curs.fetchall()
    #This is how we separate the different dataset in one gnuplot data file
    f.write('\n\n{}\n'.format(name[0]))
    for row in avgs:
        monday = row[0]
        avg = float(row[1])
        #Separating today's data from the output
        temps = [float(i[1]) for i in data if i[0] == monday]
        #Separating the upper and lower half
        up = [temp for temp in temps if temp > avg]
        down = [temp for temp in temps if temp < avg]
        f.write('{}|{:f}'.format(monday,avg))
        for i in range(resolution):
            f.write('|{:f}'.format(down[int(len(down)*((i)/resolution))]))
        for i in range(resolution):
            f.write('|{:f}'.format(up[int(len(up)*((i+1)/resolution))-1]))
        f.write('\n')
f.close()

f = open('/tmp/yearfog.gpl','w')

startday = datetime.datetime.today() - datetime.timedelta(days = daysback)

f.write('''
load 'shared.gpl'

set timefmt '%Y-%m-%d'
set xdata time
set xlabel 'Months'
set ylabel 'Temp (C)'
set xrange ['{}':]
set style fill transparent solid {:f} noborder
set xtics ('''.format(startday.strftime('%F'), 1/resolution))

day = datetime.datetime.today()
for i in range(months):
    day = day.replace(day=1, hour=0, minute=0, second=0, microsecond=0)
    f.write('"{}" {}'.format(day.strftime('%b'), day.strftime('%s')))
    if months - i > 1:
        f.write(', ')
    day = day - datetime.timedelta(days=1)
f.write(')\n\n')
f.write('plot for [IDX=0:5] \'/tmp/yearfog.txt\' i IDX u 1:2 w l ls (IDX+1) lw 2 title columnheader,\\\n')
for i in range(resolution):
    f.write('for [IDX=0:5] \'/tmp/yearfog.txt\' i IDX u 1:{:d}:{:d} w filledcurve ls (IDX+1) notitle'.format(i+3, i+3+resolution))
    if resolution - i > 1:
        f.write(',\\\n')
f.write('\n')

f.close()

call(['/usr/bin/gnuplot', '/tmp/yearfog.gpl'])

f.close()
