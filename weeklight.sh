#!/bin/dash

STARTTIME=$( date -d "8 days ago" +%F )

sqlite3 /opt/csaszar/templog/temp.db '
select 
    strftime("%Y-%m-%d %H",epoch,"unixepoch","localtime") as hour,
    sum(temp)/count(temp) 
from 
    log 
where 
    name="slux" 
    and 
    epoch > strftime("%s","now","localtime","-9 day") 
group by 
    hour;
' > /tmp/weeklight.txt

gnuplot -e "starttime=\"$STARTTIME\"" weeklight.gpl |
./note.sh > static/weeklight.png

