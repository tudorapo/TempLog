#!/bin/sh
BACKUPTIME=$( date +%F )
cd $( dirname $0 )
sqlite3 temp.db .dump | bzip2 > /storage/mentes/tempdb/temp-"$BACKUPTIME".sqlz2
pg_dump -d envlog -h localhost -U backup | bzip2 > /storage/mentes/tempdb/envlog-"$BACKUPTIME".sqlz2
