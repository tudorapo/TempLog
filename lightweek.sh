#!/bin/dash
sqlite3 temp.db '
select strftime("%H",epoch,"unixepoch","localtime"), strftime("%Y%m%d%H",epoch,"unixepoch","localtime") as hour, sum(temp)/count(temp), date(epoch,"unixepoch","localtime") from log where name = "slux" and epoch > strftime("%s","now","localtime","-30 days","start of day") group by hour order by epoch;
' | awk -F'|' -f header.awk > /tmp/lightweek.txt
gnuplot lightweek.gpl 2>/dev/null | 
./note.sh > static/lightweek.png

