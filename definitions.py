from peewee import *
from secret import database
import datetime as dt
import matplotlib.dates as mdates
import pytz

mytz = pytz.timezone('Europe/Budapest')


class UnknownField(object):
    def __init__(self, *_, **__): pass

class BaseModel(Model):
    class Meta:
        database = database

class Log(BaseModel):
    epoch = IntegerField(null=True)
    insdate = DateTimeField(constraints=[SQL("DEFAULT now()")], index=True, null=True)
    ip = TextField(index=True, null=True)
    name = TextField(null=True)
    temp = FloatField(null=True)
    type = TextField(null=True)

    class Meta:
        table_name = 'log'
        primary_key = False

class Units(BaseModel):
    log = BooleanField(constraints=[SQL("DEFAULT false")], null=True)
    type = TextField(null=True)
    unit = TextField(null=True)
    title = TextField(null=True)

    class Meta:
        table_name = 'units'
        primary_key = False

drawparams = { 
        'day': {'name':'last day', 
            'timespan':dt.timedelta(days=1,hours=1), 
            'majtick':mdates.HourLocator(byhour=list(range(0,23,2))), 
            'mintick':mdates.MinuteLocator(byminute=[0,30]), 
            'tickfmt':mdates.DateFormatter("%a/%H:%M",tz = mytz), 
            'trunc':'minute' },
        'week': {'name':'last week', 
            'timespan':dt.timedelta(days=8), 
            'majtick':mdates.DayLocator(), 
            'mintick':mdates.HourLocator(byhour=[0,3,6,9,12,15,18,21]), 
            'tickfmt':mdates.DateFormatter("%a", tz = mytz), 
            'trunc':'hour'},
        'month': {'name':'last two months', 
            'timespan':dt.timedelta(days=90), 
            'majtick':mdates.WeekdayLocator(byweekday=mdates.MO), 
            'mintick':mdates.DayLocator(), 
            'tickfmt':mdates.DateFormatter("%b/%d", tz = mytz), 
            'trunc':'day' },
        'year': {'name':'last year', 
            'timespan':dt.timedelta(days=400), 
            'majtick':mdates.MonthLocator(bymonthday=1), 
            'mintick':mdates.WeekdayLocator(mdates.MO), 
            'tickfmt':mdates.DateFormatter("%Y/%b", tz = mytz), 
            'trunc':'week' }
        }
