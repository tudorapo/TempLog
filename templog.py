"""
Accept the data, write it to a spool file.
"""
from __future__ import print_function
import time
import os
import json
import tempfile
import re
from flask import Flask
from flask import request

import config

app = Flask(__name__)

@app.route("/")
def hello():
    """Opening page with minimal information."""
    return 'Hi!'

@app.route("/add/<name>/<temp>")
@app.route("/add/<name>/<category>/<temp>")
def add(name, temp, category='dunno'):
    """Writing to a spool file."""
    data = {}
    data['epoch'] = time.time()
    try:
        data['clientip'] = re.sub(
            '[^.:0-9]', '',
            request.environ['HTTP_X_FORWARDED_FOR'])
    except KeyError:
        data['clientip'] = re.sub(
            '[^.:0-9]', '',
            request.environ['REMOTE_ADDR'])
    data['name'] = re.sub('[^a-z0-9]', '', name)
    data['category'] = re.sub('[^a-z0-9]', '', category)
    data['temp'] = temp
    fd, outfile = tempfile.mkstemp(suffix='.json', dir=config.SPOOLDIR)
    with open(outfile, 'w') as fo:
        json.dump(data, fo)
    os.close(fd)
    return json.dumps(data, indent=4, separators=(',', ': '))

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8081)
