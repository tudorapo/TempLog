#!/bin/sh
cd $( dirname $0 )
sqlite3 temp.db < gauss.sql |
awk -F'|' -f header.awk > /tmp/gauss.txt ; 
gnuplot gauss.gpl 2>/dev/null | 
./note.sh > static/gauss.png
