create temporary table gauss as select 
    round(temp*2,0)/2 as szam, 
    count(*) as darab, 
    name 
from log  
where type = 'temp'
group by szam,name
order by name,szam;

create temporary table gaussmax as select 
    max(darab) as max,
    name 
from gauss 
group by name 
order by name; 

select 
    gauss.szam,
    (cast(gauss.darab as float)/cast(gaussmax.max as float))*100,
    gauss.name 
from gauss, gaussmax 
where gauss.name=gaussmax.name;
