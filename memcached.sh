#/bin/sh
echo "stats slabs" | 
    netcat -q 1 localhost 11211 | 
    cut -d: -f1 | 
    cut -d" " -f2 | 
    sort -nu | 
    grep "^[0-9]*$" | 
    while read item ; do 
        echo "Item $item" ; 
        echo "stats cachedump $item 0" | 
            netcat -q 1 localhost 11211 ; 
        done

