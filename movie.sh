#!/bin/bash
cd /opt/csaszar/templog
mkdir -p /tmp/gaussmovie
DBFILE=temp.db
RADIUS=3
MRADIUS=$(($RADIUS + 1))

#Creating the list of start and stop seconds and the date of the actual day
cat <<-SQL1OF |
    select distinct 
        strftime("%s",epoch,"unixepoch","localtime","-$RADIUS days","start of day") as start, 
        strftime("%s",epoch,"unixepoch","localtime","+$MRADIUS days","start of day")-1 as end,
        date(epoch,"unixepoch","localtime") as title
    from log 
    order by end;
SQL1OF
sqlite3 -separator ' ' $DBFILE | 

while read -r START END TITLE; do
    #Creating one diagrams data, ending in a txt file
    cat <<- SQL2OF |
        -- Creating the columns by half celsius precision
        create temporary table gauss as 
            select 
                round(temp*2,0)/2 as szam, 
                count(*) as darab, 
                name 
            from log  
            where epoch > $START and epoch < $END
            group by szam,name
            order by name,szam;
        -- Searching the maximum values for percent generation
        create temporary table gaussmax as 
            select 
                max(darab) as max,
                 name 
            from gauss 
            group by name 
            order by name; 
        -- Converting the first table to percentages from raw values
        select 
            gauss.szam,
            (cast(gauss.darab as float)/cast(gaussmax.max as float))*100,
            gauss.name 
        from gauss, gaussmax 
        where gauss.name=gaussmax.name;
SQL2OF
    sqlite3 $DBFILE | awk -F'|' -f header.awk > /tmp/"$TITLE".txt
    
    #Creating the Gnuplot file for the above txt file
    cat shared.gpl > /tmp/"$TITLE".gpl
    cat <<- GPLOF >> /tmp/"$TITLE".gpl
        set title 'Temperature range and distribution around $TITLE'
        set timefmt '%s'
        set xlabel 'Temp (C)'
        set ylabel 'Oftennes (percent of max)'
        set xrange [-10:40]
        set boxwidth 0.8 relative
        set xtics 5
        set style fill transparent solid 0.75 noborder
        plot for [IDX=0:4] '/tmp/$TITLE.txt' i IDX using 1:2 w boxes ls (IDX+1) lw 2 title columnheader
GPLOF
    #Drawing a picture
    gnuplot /tmp/"$TITLE".gpl 2>/tmp/movie.log > /tmp/gaussmovie/"$TITLE".png
    #rm /tmp/"$TITLE".gpl /tmp/"$TITLE".txt
done

#Converting the pictures to an animated gif
convert -delay 20 -loop 0 /tmp/gaussmovie/*png /storage/teszt.gif
