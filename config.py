"""
Some configuration values
"""
import os
BASEDIR = os.path.dirname(os.path.abspath(__file__))
SPOOLDIR = os.path.join(BASEDIR, 'spool')
MYDB = os.path.join(BASEDIR, 'temp.db')
SEARCH = os.path.join(SPOOLDIR, '*.json')
PG_DB = 'envlog'
PG_USER = 'envlog'
PG_HOST = 'psql'
PG_PW = 'alma'
