#!/bin/sh
cd $( dirname $0 )
TIME=$(( $( date +%s ) - $1 ))
. ./tdiff.sh
sqlite3 temp.db "select epoch+"$TDIFF", temp, name from log where epoch > "$TIME" and type = 'temp' order by name,epoch;"  | 
awk -F'|' -f header.awk > /tmp/temp.txt
gnuplot temp.gpl 2>/dev/null | ./note.sh > static/temp-"$2".png 2> /dev/null
