#!/bin/sh
exec 2> /tmp/year.log

cd $( dirname $0 )

TIME=$( date -d "32 days ago" +%s )
. ./tdiff.sh

STARTTIME=$( date -d "13 months ago" +%F )

cat << 'EOF1' > /tmp/year.gpl
load 'shared.gpl'

set timefmt '%Y-%m-%d'
set xdata time
set xlabel 'Months'
set ylabel 'Temp (C)'
set xtics \
EOF1

echo -n "(" >> /tmp/year.gpl
for DAY in `seq 450 -1 1` ; do
    test $( date -d "$DAY days ago" +%e ) -ne "1" || date -d "$DAY days ago" +\"%b\",%s;
done | tr "," " "| tr "\n" "," | sed "s/,$/)/" >> /tmp/year.gpl

cat << 'EOF2' >> /tmp/year.gpl

set xrange [starttime:]

plot for [IDX=0:5] '/tmp/year.txt' i IDX using 1:3 with lines linestyle (IDX+1) linewidth 2 title columnheader,\
     for [IDX=0:5] '/tmp/year.txt' i IDX using 1:2:4 with filledcurve linestyle (IDX+1) notitle
EOF2

sqlite3 temp.db '
select 
	strftime("%Y-%m-%d",min(epoch)+'$TDIFF',"unixepoch"),
	min(temp), 
	avg(temp),
	max (temp), 
	strftime("%Y%W",epoch+'$TDIFF',"unixepoch") as week,
	name
from log 
where type = '\''temp'\''
group by name,week 
order by name,week;'|
awk -F'|' -f header.awk > /tmp/year.txt
gnuplot -e "starttime=\"$STARTTIME\";" /tmp/year.gpl | 
./note.sh > static/year.png

