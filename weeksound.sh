#!/bin/sh
echo $0
cd $( dirname $0 )

STARTTIME=$( date -d "8 days ago" +%F )

TIME=$( date -d "10 days ago" +%s )
. ./tdiff.sh

sqlite3 temp.db '
begin transaction;
create temporary table if not exists sound as select strftime("%Y-%m-%d:%H:00",epoch+'$TDIFF',"unixepoch") as hour from log where type='\''snd'\'' and epoch > '$TIME' group by hour;
alter table sound add column max int;
alter table sound add column min int;
alter table sound add column avg int;
create temporary table if not exists sound_max as select strftime("%Y-%m-%d:%H:00",epoch+'$TDIFF',"unixepoch") as hour, sum(temp)/count(temp) as max, name from log where type='\''snd'\'' and name='\''max'\'' group by hour,name;
create temporary table if not exists sound_min as select strftime("%Y-%m-%d:%H:00",epoch+'$TDIFF',"unixepoch") as hour, sum(temp)/count(temp) as min, name from log where type='\''snd'\'' and name='\''min'\'' group by hour,name;
create temporary table if not exists sound_avg as select strftime("%Y-%m-%d:%H:00",epoch+'$TDIFF',"unixepoch") as hour, sum(temp)/count(temp) as avg, name from log where type='\''snd'\'' and name='\''avg'\'' group by hour,name;
update sound set max = (select max from sound_max where sound.hour = sound_max.hour);
update sound set min = (select min from sound_min where sound.hour = sound_min.hour);
update sound set avg = (select avg from sound_avg where sound.hour = sound_avg.hour);
select * from sound order by hour;
commit;' > /tmp/weeksound.txt
gnuplot -e "starttime=\"$STARTTIME\"" weeksound.gpl | 
./note.sh > static/weeksound.png 

