#!/bin/sh
cd $( dirname $0 )
TIME=$(( $( date +%s ) - $1 ))
. ./tdiff.sh
sqlite3 temp.db "select epoch+"$TDIFF", temp, name from log where epoch > "$TIME" and type = 'snd' order by name,epoch;"  | 
awk -F'|' -f header.awk > /tmp/snd.txt
gnuplot snd.gpl | ./note.sh > static/snd-"$2".png 2> /dev/null
