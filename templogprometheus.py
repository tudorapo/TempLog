#!/usr/bin/env python3
"""
Provide templog ddata for prometheus
"""
import json
import configparser
import logging
import argparse
from http.server import BaseHTTPRequestHandler, HTTPServer
from pymemcache.client.base import Client

logging.basicConfig(level=logging.INFO)
parser = argparse.ArgumentParser(description='Turbostat for prometheus.')
parser.add_argument('-a', '--address', default='0.0.0.0',
                    help='ip address where to serve')
parser.add_argument('-p', '--port', default='9101',
                    help='Port where to serve')
args = parser.parse_args()

config = configparser.ConfigParser()
config.read('config.conf')

class Metrics(BaseHTTPRequestHandler):
    """Simple webserver."""
    def hwrite(self, msg):
        """for convenience."""
        self.wfile.write(bytes(msg, 'utf-8'))
    def log_request(self, code='-', size='-'):
        if code != 200:
            self.log_message('jaj! "%s" %s %s',self.requestline, str(code), str(size))
    def do_GET(self):
        """http reply."""
        self.send_response(200)
        self.send_header('Content-type', 'text/plain; version=0.0.4; charset=utf-8')
        self.end_headers()
        cache = Client(f"{ config['db']['mchost'] }:{ config['db']['mcport'] }")
        nevsorraw = json.loads(cache.get('robot-nevsor').decode('utf-8'))
        unitsraw = json.loads(cache.get('robot-units').decode('utf-8'))
        proms = []
        for pair in nevsorraw:
            proms.append(f"{ config['path']['prefix'] }-{ pair[0] }-{ pair[1] }")
        values = cache.get_many(proms)
        cache.close()
        fn = ''
        for name,value in values.items():
            names = name.split('-')
            tmp = f'{ names[0] }_{ names[1] }'
            if fn != tmp:
                fn = tmp
                self.hwrite(f'# HELP { fn } { unitsraw[names[1]][2] } { unitsraw[names[1]][1] }\n')
                self.hwrite(f'# TYPE { fn } gauge\n')
            self.hwrite(f'{ fn }{{room="{ names[2] }"}} { value.decode("utf-8") }\n')

server = HTTPServer((args.address, int(args.port)), Metrics)
logging.info('webserver starts at %s/%s', args.address, args.port)
try:
    server.serve_forever()
except KeyboardInterrupt:
    pass
server.server_close()
