"""
Updates the list of sensors based on the logged data.
"""
from __future__ import print_function
import argparse
from pymemcache.client.base import Client
import psycopg
import json

parser = argparse.ArgumentParser()
parser.add_argument('-f', '--force',
        help='Force recreating the memcache values',
        action='store_true')
args = parser.parse_args()

cache = Client('127.0.0.1:11211')
units = cache.get('robot-units')

if units and  not args.force:
    print('Stuff is there, bailing')
    quit()
else:
    print('Stuff is not there, working')

pconn = psycopg.connect("dbname='envlog' user='envlog' host='localhost' password='alma'")

pcur = pconn.cursor()
pcur.execute('''
        drop table nevsor; 
        select log.type, log.name 
        into nevsor 
        from log, units 
        where log.type = units.type 
        group by log.type, log.name;''')
pcur.execute('select * from units order by type desc;')
units = {}
for line in pcur:
    units[line[0]] = line
pcur.execute('select * from nevsor order by type,name;')
nevsor = pcur.fetchall()
cache.set('robot-units', json.dumps(units), 60*60*3)
cache.set('robot-nevsor', json.dumps(nevsor), 60*60*3)
pconn.close()
