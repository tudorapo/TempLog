#!/usr/bin/env python3
"""
This script is to read in the json files from the spool directory,
do some work like ip check and averaging multiple values then
put it into sqlite and postgresql.
"""
import time
import os
import glob
import sqlite3
import json
import configparser
import logging
import psycopg
from pymemcache.client.base import Client

def tempavg(temp):
    """
    For added complectosimplicity, when a device has to send in several
    samples for one measurement we do the averaging here and not in
    Arduino.
    """
    templist = []
    #Croaked sensor
    if temp in ('NaN', 'nan'):
        return 0
    #Converting numbers to avoid injections
    for number in temp.split(','):
        try:
            templist.append(float(number))
        except ValueError:
            pass
    #If we got a list we sort, cut the two extremes then average.
    if len(templist) > 3:
        templist.sort()
        del templist[len(templist) - 1]
        del templist[0]
        summ = 0
        for i in templist:
            summ = summ + float(i)
        bubu = summ / len(templist)
        return str(bubu)
    #No data
    if not templist:
        logging.error('Faulty data: %s', temp)
        return 0
    #If we dont we just take the number
    return templist[0]

def checkip(memcache, ipname, clientip):
    """
    Separating this test because pylint is anal.
    """
    oldip = memcache.get(ipname)
    if not oldip:
        logging.info('ip cache update: %s will be %s', ipname, clientip)
        if not memcache.set(ipname, clientip, 2591000):
            logging.error('o bazmeg')
    elif oldip.decode('utf-8') != clientip:
        return False
    return True

BASEDIR = os.path.dirname(os.path.abspath(__file__))
CONFIGPATH = os.path.join(BASEDIR, 'config.conf')

config = configparser.ConfigParser()
config.read(CONFIGPATH)

MYDB = os.path.join(BASEDIR, config['db']['sqlite'])
SEARCH = os.path.join(BASEDIR, 'spool/*.json')

logging.basicConfig(level=logging.DEBUG)

while True:
    filelist = glob.glob(SEARCH)
    logging.info('Number of files to ingest: %d', len(filelist))
    cache = Client(f"{ config['db']['mchost'] }:{ config['db']['mcport'] }")
    pconn = psycopg.connect(
            f"""dbname='{ config['db']['pgdb'] }' 
            user='{ config['db']['pguser'] }' 
            host='{ config['db']['pghost'] }' 
            password='{ config['db']['pgpw'] }'""")
    pcur = pconn.cursor()
    sconn = sqlite3.connect(MYDB)
    scur = sconn.cursor()
    for infile in filelist:
        tmpfile = infile.replace('json', 'tmp')
        os.rename(infile, tmpfile)
        with open(tmpfile, 'r', encoding='utf-8') as jf:
            try:
                data = json.load(jf)
            except json.decoder.JSONDecodeError:
                logging.warning('Incorrect json file %s', tmpfile)
                continue
        if 'category' not in data:
            data['category'] = 'dunno'
        newtemp = tempavg(data['temp'])
        keyname = f"ip-{data['name'] }-{ data['category'] }"
        if not checkip(cache, keyname, data['clientip']):
            logging.warning('%s, %s does not match, dropped.', keyname, data['clientip'])
            continue
        if data['category'] == 'temp' and newtemp == 0:
            logging.warning('Data from %s contains zero temp, dropped.', data['clientip'])
        elif data['category'] == 'powr' and data['name'] == 'desktop' and newtemp < 0:
            logging.warning('Desktop power data from %s contains irreal number, dropped.',
                            data['clientip'])
        else:
            pvalues = (
                data['epoch'],
                data['clientip'],
                data['name'],
                newtemp,
                data['category'],
                data['epoch'])
            svalues = (
                data['epoch'],
                data['clientip'],
                data['name'],
                newtemp,
                data['category'])
            scur.execute('insert into log values (?,?,?,?,?);', svalues)
            pcur.execute('''
                insert into log 
                values (%s,%s,%s,%s,%s,to_timestamp(%s));
                ''', pvalues)
        oldfile = tmpfile.replace('tmp', 'done')
        promname = f"{ config['path']['prefix'] }-{ data['category'] }-{ data['name'] }"
        cache.set(promname, newtemp, 300)
        logging.debug('memc: %s,%s', promname, newtemp)
        os.rename(tmpfile, oldfile)
        if data['category'] == 'temp' or data['category'] == 'hum':
            cache.delete('latest')
    sconn.commit()
    pconn.commit()
    sconn.close()
    pconn.close()
    logging.info('Waiting now')
    time.sleep(45)
