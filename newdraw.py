# -*- coding: utf-8 -*-
import pytz
import argparse
import time
import logging
import datetime as dt


import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.offsetbox import AnchoredText
import numpy as nu
import matplotlib.ticker as plticker


#The imports are messed up. I wanted to hide the pw/user from the code, which made this horror. sry.
from definitions import *

def perf(do, st, ct, tx):
    now = time.time()
    if do:
        print('After {:10} {:6.2f} {:6.2f}'.format(tx, now-st, now-ct))
    return now

def debug(do, message):
    if do:
        print(message)

logger = logging.getLogger('peewee')
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.INFO)

parser = argparse.ArgumentParser()
parser.add_argument('-t', '--type', 
        help='Type of sensor to draw')
parser.add_argument('-n', '--name', 
        help='name of sensor to draw')
parser.add_argument('-s', '--slice', 
        help='Timeslice to draw')
parser.add_argument('-p', '--performance', 
        help='Writes out iming info', action='store_true')
parser.add_argument('-d', '--debug', 
        help='Writes out SQL sent to the server', action='store_true')
args = parser.parse_args()

if args.debug:
    logger.setLevel(logging.DEBUG)

#QUERY: because the timezone has to be set everyhere. 
database.execute_sql('SET TIME ZONE \'CET\';')

starttime = time.time()
lasttime = starttime

#QUERY: collecting the units information from sql

units = {}
unitdata = Units.select()
for unit in unitdata:
    units[unit.type]=(unit.unit, unit.title, unit.log)

lasttime = perf(args.performance, starttime, lasttime, 'units')

for drawidx, draw in drawparams.items():
    if args.slice and args.slice != drawidx:
        debug(args.debug,'Skipping {}'.format(drawidx))
        continue
    #QUERY: collecting the types and names 
    data =  Log.select(
            Log.name, 
            Log.type
        ).join(
            Units,
            on=(Units.type == Log.type)
        ).distinct(
        ).where(
            Log.insdate > (dt.datetime.today() - draw['timespan'])
        )
    lasttime = perf(args.performance, starttime, lasttime, 'names')
    #Creating the multidim array to keep the lists
    linez = {}
    for record in data:
        if not record.type in linez:
            linez[record.type] = {}
        if not record.name in linez[record.type]:
            linez[record.type][record.name] = {}
    #QUERY: collecting the actual data
    data =  Log.select(
            Log.name, 
            Log.type, 
            fn.COUNT(Log.temp).alias('count'), 
            fn.MAX(Log.temp).alias('max'),
            fn.MIN(Log.temp).alias('min'),
            fn.AVG(Log.temp).alias('avg'),
            fn.DATE_TRUNC(draw['trunc'], Log.insdate).alias('timeslice')
        ).join(
            Units,
            on=(Units.type == Log.type)
        ).where(
            Log.insdate > (dt.datetime.today() - draw['timespan'])
        ).order_by(
            Log.name,
            SQL('timeslice').desc()
        ).group_by(
            Log.type,
            Log.name,
            SQL('timeslice')
        )
    #Cheating to get all the data in one swoop
    d2 = data.dicts()
    lasttime = perf(args.performance, starttime, lasttime, 'sql')
    #Timestamps have to be converted to the internal mpl timeformat
    tmax = mdates.date2num(dt.datetime.min)
    tmin = mdates.date2num(dt.datetime.max)
    #Filtering out the lists to draw from
    for dtype in linez:
        for dname in linez[dtype]:
            linez[dtype][dname]['avg'] = map(lambda f: f['avg'], filter(lambda d: d['name'] == dname and d['type'] == dtype, d2))
            linez[dtype][dname]['min'] = map(lambda f: f['min'], filter(lambda d: d['name'] == dname and d['type'] == dtype, d2))
            linez[dtype][dname]['max'] = map(lambda f: f['max'], filter(lambda d: d['name'] == dname and d['type'] == dtype, d2))
            linez[dtype][dname]['tim'] = map(lambda f: mdates.date2num(f['timeslice']), filter(lambda d: d['name'] == dname and d['type'] == dtype, d2))
            tmin=min(tmin,min(linez[dtype][dname]['tim']))
            tmax=max(tmax,max(linez[dtype][dname]['tim']))
    lasttime = perf(args.performance, starttime, lasttime, 'maps')
    #Start to draw!
    for drawtype in linez:
        if args.type and args.type != drawtype:
            debug(args.debug,'Skipping {}'.format(drawtype))
            continue
        c = 0
        fig = plt.figure(figsize=(16,9), dpi=100)
        ax = fig.add_subplot(111)
        ax.xaxis_date('Europe/Budapest')
        ax.xaxis.set_major_locator(draw['majtick'])
        ax.xaxis.set_minor_locator(draw['mintick'])
        ax.xaxis.set_major_formatter(draw['tickfmt'])
        ax.set_xlim(tmin,tmax)
        ax.set_title('{} in the {}.'.format(units[drawtype][1],draw['name']))
        ax.set_ylabel('{} ({})'.format(units[drawtype][1],units[drawtype][0]))
        ax.grid(which='major',color='lightgrey',zorder=2)
        ax.grid(which='minor',axis='y',color='lightgrey',linestyle=':',zorder=3)
        ax.set_axisbelow(True)
        ax.tick_params(axis='y', labelleft=True, labelright=True)
        if drawtype == 'temp':
            loc = plticker.MultipleLocator(base=5.0)
            ax.yaxis.set_major_locator(loc)
            locm = plticker.MultipleLocator(base=1.0)
            ax.yaxis.set_minor_locator(locm)
        ax.annotate(u'© Tomka Gergely, generated at {}'.
                format(dt.datetime.now(pytz.timezone('Europe/Budapest')).
                strftime('%Y/%m/%d %H:%M:%S')),
                xy=(1,0),
                xycoords='figure fraction',
                annotation_clip=False,
                va='bottom',
                ha='right',
                size='smaller')
        filename = 'static/new_{}_{}.png'.format(drawtype, drawidx)
        if units[drawtype][2]:
            ax.set_yscale('log')
        for drawname in sorted(linez[drawtype]):
            if args.name and args.name != drawname:
                debug(args.debug,'Skipping {}'.format(drawname))
                continue
            elif args.name == drawname:
                filename = 'static/new_{}_{}_{}.png'.format(drawtype, drawidx, drawname)
            color='C{:d}'.format(c)
            c = c + 1
            ax.plot(linez[drawtype][drawname]['tim'], 
                    linez[drawtype][drawname]['avg'],
                    color=color,
                    label=drawname)
            ax.fill_between(
                    linez[drawtype][drawname]['tim'],
                    linez[drawtype][drawname]['avg'],
                    linez[drawtype][drawname]['max'],
                    alpha=0.2,
                    color=color)
            ax.fill_between(
                    linez[drawtype][drawname]['tim'],
                    linez[drawtype][drawname]['avg'],
                    linez[drawtype][drawname]['min'],
                    alpha=0.2,
                    color=color)
        ax.legend(loc='upper right',framealpha=0.4)
        fig.tight_layout()
        fig.savefig(filename)
        plt.close(fig)
        lasttime = perf(args.performance,starttime,lasttime,'draw')
