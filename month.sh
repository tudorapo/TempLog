#!/bin/sh

cd $( dirname $0 )

TIME=$( date -d "32 days ago" +%s )
. ./tdiff.sh

STARTTIME=$( date -d "32 days ago" +%F )

#To avoid too dense dates on the x axis the main tics must be per week
#And to make sense of that they have to start on Monday
#Moving backward in time, because the first monday of the chart
#must be before the start of the charts, or there will be no tics
#before the first monday.

COUNTER=32 
while 
    test $( date -d "$COUNTER days ago" +%u ) -ne "1" ; 
do 
    COUNTER=$(( COUNTER += 1 )) 
done 
MONDAY=$( date -d "$COUNTER days ago" +%F )

sqlite3 temp.db '
select 
    strftime("%Y-%m-%d",epoch+'$TDIFF',"unixepoch") as day,
    min(temp),
    sum(temp)/count(temp),
    max(temp),
    name 
from log 
    where 
        epoch > "'$TIME'" 
        and
        type = '\''temp'\''
    group by name,day 
    order by name,day;' | 
awk -F'|' -f header.awk > /tmp/month.txt
sqlite3 temp.db '
select 
    day,
    avg(temp) 
    from (
        select 
            strftime("%Y-%m-%d",epoch+'$TDIFF',"unixepoch") as day, 
            strftime("%H",epoch+'$TDIFF',"unixepoch") as hour, 
            temp 
        from log 
        where 
            name="balcony" and (
                hour="01" or 
                hour="07" or 
                hour="13" or 
                hour="19"
            ) 
       group by day,hour 
       having min(rowid) 
       order by day,hour) 
   group by day 
   order by day,hour;' > /tmp/offavg.txt
gnuplot -e "starttime=\"$STARTTIME\"; monday=\"$MONDAY\"" month.gpl 2>/dev/null | 
./note.sh > static/month.png 
