export ADAT=/tmp/adat.txt
for month in `seq 1 12`  ; do 
    echo; 
    echo; 
    LANG=hu_HU.UTF-8 date -d "2016-$month-01" +%B; 
    psql -z -t -h psql -U envlog envlog -c "
        select 
            to_char(date_part('hour', insdate),'FM00') || ':' ||  to_char(floor(date_part('minute', insdate) / 10)*10,'FM00') as time,
            avg(temp) 
        from log 
        where 
            name='street' 
            and 
            date_part('month',insdate)  = $month 
        group by time 
        order by time;
    " ; done > $ADAT
echo "\n\negész év" >> $ADAT
psql -z -t -h psql -U envlog envlog -c "
    select 
        to_char(date_part('hour', insdate),'FM00') || ':' ||  to_char(floor(date_part('minute', insdate) / 10)*10,'FM00') as time,
        avg(temp) 
    from log 
    where name='street'
    group by time 
    order by time;
" >> $ADAT 
