# Silly temperature logging thing

I made this thing for several reasons:

   * To play with python flask, which is crap
   * To see how sqlite3 grows
   * To make complicated sql queries
   * To make complicated GnuPlot diagrams

Basically, to learn. And learn I did!

## Files

With some explanation.

**Bold** items are still valid, others are outdated.

### Generic stuff

   * **dark2.pal**: A color palette stolen from the [gnuplot palettes](https://github.com/Gnuplotting/gnuplot-palettes) project.
   * **header.awk**: Awk thingy to separate data to sources, can't do this from sqlite2.
   * **note.sh**: Puts the date onto the images.
   * **shared.gpl**: Commong GnuPlot configs.
   * **newdefaults.gpl**: Commong GnuPlot configs for the new python drawer.
   * **tdiff.sh**: Time zone difference in seconds, as the time stored in the db is in UTC.
   * **toplist.sql**: Postgresql to calculate the record temperature, humidity and pressure values (outdated)
   * pltest.sql: Definition of a PL/PGSQL function to create the record table.
   * **toplist.sh**: Wrapper script to run the above function.
   * **definitions.py**: Some function and drawing definitions.
   * **ingest.py**: Script to put the data from the spool files to the dbs.
   * **templogprometheus.py**: Prometheus exporter

### Drawing diagrams 
The daily, weekly and monthly are different in small ways, like the horizontal axis is marked in hours or days, so there are separate script and configs.

   * **drawsmart.sh**: Draws the few hourly diagrams.
   * **temp.gpl**: GnuPlot to draw the few hourlies.
   * **week.gpl**: Draws the weekly diagram.
   * **week.sh**: GnuPlot to draw the weekly diagram.
   * **month.gpl**: Draws the few monthly diagram.
   * **month.sh**: GnuPlot to draw the monthly diagram.
   * **gauss.sh**: Draws the temperature distribution diagram. I hereby reject every question about why this diagram is necessary.
   * **gauss.gpl**: GnuPlot to draw the above.
   * **gauss.sql**: To generate the data a complicated sql was necessary so it's in a separate file.
   * **movie.sh**: Generates an animgif of gaussian diagrams of one week pieces of the log.
   * **mindenfele.tmpl**: Templates for the new python drawer.
   * **draw.py**: The obsolete python drawer.
   * **pydraw.sh**: Theobsolete python drawer wrapper.
   * **newdraw.py**: The new python drawer.
   * **newpydraw.sh**: The new python drawer wrapper.
   * **colormaps.py**: Two stolen colormaps. No ragrets.
   * **scaledraw.py**: A script to draw diagrams showing the distribution of values in a given interval (week).
   * **fogdraw.py**: A script to draw diagrams showing the distribution of values in a given interval (week).
   * **dailyaverage.sh**: Had to answer a question about the lowest temperature of the day.


### Flask things

   * temp.ini: The UWSGI config. The fun part the "mount" option which is needed because the flaks is not at the html root of my nginx.
   * temp.py: The Flask app itself. I'm not using the builting flasql thingy.
   * **weblog.sh**: Example scriptlet to show how to report temperature to Flask.
   * **templates**: Directory for HTML templates.
   * robotindex.sh: Generates a html template for the diagrams drawn by the python drawer.
   * **templog.py**: Flask service which accepts data from the sensors and puts it into files.
   * **showtemp.py**: Flask service which displays the various charts.

## TODO

   - Make ingest runittable, mostly to return from bad ctrlcs
   - pylint, things are messi now
   - clean up templog, it may has a lot of unnecessary modules and html headers

## Other

### Nginx

Snippet from the nginx default site config, from the server section:

```
location /temp/ { #@yourapplication {
        include uwsgi_params;
        uwsgi_pass unix:/var/tmp/templog/temp_uwsgi.sock;
}
```

### Runit

Bad, very bad run script for the flask thingy and runit. The uswgi does not wants to play properly.

```
#!/bin/sh
exec 2>&1
ps axfu | grep uwsgi | grep -v grep | awk '{print $2}' | xargs kill
. /var/tmp/templog/venv/bin/activate
chpst -u tudor uwsgi --exit-on-reload --die-on-term --honour-stdin --ini /var/tmp/templog/temp.ini
```

Snippet to find holes in the pressure data:
```
select epoch,name,diff,pepoch from (select epoch, name, temp - lag(temp) over (partition by name order by epoch) as diff, lag(epoch) over (partition by name order by epoch) as pepoch from log where type = 'atm') as diffs where abs(diff) > 2;
```

### Pip

Pip install these packages to make it work: pylibmc pytz matplotlib peewee psycopg2-binary uWSGI flask pylibmc

### Postgresql

Adding a backup user:

   1. Became a postgres superuser (su - sql ; lxc-attach psql ; su - postgres ; psql envlog)
   1. CREATE USER backup PASSWORD 'alma';
   1. GRANT SELECT ON ALL TABLES IN SCHEMA public TO backup;
   1. GRANT SELECT ON ALL SEQUENCES IN SCHEMA public TO backup;
   1. Edit /etc/postgresql/pg_hba.conf to your liking
   1. SELECT pg_reload_conf();

### Adding a new unity/type

   1. After there is some data in the db,
   1. Add the new units to the units table
   1. run update_units.py with -f 
