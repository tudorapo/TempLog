import psycopg2
import matplotlib as mpl
mpl.use('Agg')
from matplotlib.dates import date2num, MonthLocator, WeekdayLocator, DateFormatter, MO
from matplotlib.pyplot import figure, close
from datetime import datetime

n = 20
q1 = 'SELECT name, type, date_trunc(\'week\',insdate) as timeslice, array['
q2 = '] AS perc FROM log GROUP BY name, type, timeslice ORDER BY type,name,timeslice;'
color = 'C0'
p = 0.5/n
perc=[0.5]
for i in range(n):
    perc.append(0.5 - (p * (i + 1)))
for i in range(n):
    perc.append(0.5 + (p * (i + 1)))
query = q1
for p in perc:
    query = query + 'percentile_cont({:f}) WITHIN GROUP (ORDER BY temp), '.format(p)
query = query[:-2] + q2

db = psycopg2.connect("dbname='envlog' user='envlog' host='localhost' password='alma'")
pc = db.cursor()

pc.execute('SELECT * FROM units;')
descr = {}
units = {}
for row in pc:
    units[row[0]] = row[1]
    descr[row[0]] = row[2]

pc.execute(query);
data = pc.fetchall()
db.close()
lines = {}
for row in data:
    if not row[1] in lines:
        lines[row[1]] = {}
    if not row[0] in lines[row[1]]:
        lines[row[1]][row[0]] = {}
        lines[row[1]][row[0]]['tim'] = []
        for i in range((2 * n) + 1):
            lines[row[1]][row[0]][i] = []
    lines[row[1]][row[0]]['tim'].append(date2num(row[2]))
    for i in range((2 * n) + 1):
        lines[row[1]][row[0]][i].append(row[3][i])
for dtype in lines.keys():
    try:
        #print(dtype,units[dtype],descr[dtype])
        for dname in lines[dtype].keys():
            print(dtype,dname)
            fig = figure(figsize=(16,9), dpi=100)
            ax = fig.add_subplot(111)
            ax.xaxis_date('Europe/Budapest')
            ax.xaxis.set_major_locator(MonthLocator(bymonthday=1))
            ax.xaxis.set_minor_locator(WeekdayLocator(MO))
            ax.xaxis.set_major_formatter(DateFormatter("%Y/%b"))
            ax.set_xlim(min(lines[dtype][dname]['tim']), max(lines[dtype][dname]['tim']))
            ax.set_title('{} from the {} sensor'.format(descr[dtype],dname))
            ax.set_ylabel('{} ({})'.format(descr[dtype],units[dtype]))
            ax.grid(which='major',color='lightgrey')
            ax.set_axisbelow(True)
            ax.annotate(u'(c) Tomka Gergely, generated at {}'.
                    format(datetime.now().
                    strftime('%Y/%m/%d %H:%M:%S')),
                    xy=(1,0),
                    xycoords='figure fraction',
                    annotation_clip=False,
                    va='bottom',
                    ha='right',
                    size='smaller')
            for i in range(n):
                ax.fill_between(
                        lines[dtype][dname]['tim'],
                        lines[dtype][dname][i+1],
                        lines[dtype][dname][0],
                        facecolor=color,alpha=1/float(n),linewidth=0.0)
                ax.fill_between(
                        lines[dtype][dname]['tim'],
                        lines[dtype][dname][i+1+n],
                        lines[dtype][dname][0],
                        facecolor=color,alpha=1/float(n),linewidth=0.0)
            ax.plot(
                    lines[dtype][dname]['tim'],
                    lines[dtype][dname][2*n],
                    color=color,alpha=0.1,linewidth=1.0)
            ax.plot(
                    lines[dtype][dname]['tim'],
                    lines[dtype][dname][n],
                    color=color,alpha=0.1,linewidth=1.0)
            ax.plot(
                    lines[dtype][dname]['tim'],
                    lines[dtype][dname][0],
                    color=color,alpha=1,linewidth=2.0)
            fig.tight_layout()
            fig.savefig('static/scale_{}_{}.png'.format(dtype, dname))
            close('all')
    except KeyError:
        #print("Skipping", dtype)
        pass
