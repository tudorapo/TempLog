\pset footer off

DROP TABLE IF EXISTS toplist;

CREATE TABLE toplist AS SELECT 
    records.name, 
    to_char(to_timestamp(records.epoch), 'YYYY TMMonth FMDDth, FMHH24:MI') AS date, 
    to_char(records.temp, '9999D9') || ' ' ||  units.unit AS value,
    records.temp,
    records.type,
    records.what
FROM ((
        SELECT DISTINCT ON (name, type)
            name,
            epoch,
            temp,
            type,
            'max' AS what 
        FROM log 
        WHERE type = 'snd' AND type != 'lux' AND type != 'ilux'
        ORDER BY name ASC, type, temp DESC
    ) UNION (
        SELECT DISTINCT ON (name, type) 
            name,
            epoch,
            temp,type,
            'min' AS what 
        FROM log 
        WHERE type = 'snd' AND type != 'lux' AND type != 'ilux'
        ORDER BY name ASC, type, temp ASC
)) AS records, units 
WHERE units.type = records.type 
ORDER BY records.type, records.name ASC, records.temp ASC;

SELECT name, date, value FROM toplist ORDER BY type, name, what;
