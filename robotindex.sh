#!/bin/bash

cat << HEADER
{% extends 'base.html' %}

{% block header %}
<h1>{% block title %}Enviroment sensors, all{% endblock %}</h1>
{% endblock %}

{% block content %}
HEADER

short=( temp hum lux snd atm )
long=( Temperature Humidity Light Sound Pressure )
time=( 4hr day week month year )
ltime=( "Four hours" "Last Day" "Last Week" "Last Month" "Last Year" )

echo -n "    <p>"
for i in 0 1 2 3 4 ; do
	echo -n "<a href=\"#"${short[i]}"\">"${long[i]}"</a>&nbsp;"
done
echo "<a href=\"#top\">Top</a></p>"

for i in 0 1 2 3 4 ; do
	echo "    <hr>"
	echo "    <h2 id=\""${short[i]}"\">"${long[i]}"</h2>"
	echo  -n "      <p>"
	for j in 0 1 2 3 4 ; do
		echo -n "<a href=\"#"${short[i]}${time[j]}"\">"${ltime[j]}"</a>&nbsp;"
	done
	echo "<a href=\"#top\">Top</a></p>"
	for j in 0 1 2 3 4 ; do
       	echo "        <h3 id=\""${short[i]}${time[j]}"\">"${ltime[j]}"</h3>"
        echo "        <p><img src=\"/templog/static/"${time[j]}${short[i]}".png?{{ time }}\"></p>"
        echo "        <p><a href=\"#"${short[i]}"\">Back to top</a></p>"
	done
done

cat << FOOTER
{% endblock %}
FOOTER
