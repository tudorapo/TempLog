"""
This once-simple Flask app is to display and update sensor data.
Needs a memcached, a postgresql database, templates and whatever.
"""
from __future__ import print_function
import time
import os
import re
import sqlite3
import json

import psycopg2
from werkzeug.contrib.cache import MemcachedCache
from flask import Flask
from flask import render_template
from flask import make_response
from flask import request
from flask import jsonify

import tempfile

app = Flask(__name__)
app.jinja_env.trim_blocks = True
app.jinja_env.lstrip_blocks = True

cache = MemcachedCache(['127.0.0.1:11211'])

def pgconnect():
    """Just a cheap way to avoid a global constant and to have to write once"""
    return psycopg2.connect("dbname='envlog' user='envlog' host='localhost' password='alma'")

def tempavg(temp):
    """
    For added complectosimplicity, when a device has to send in several
    samples for one measurement we do the averaging here and not in
    Arduino.
    """
    templist = []
    #Croaked sensor
    if temp == "NaN" or temp == "nan":
        return 0
    #Converting numbers to avoid injections
    for number in temp.split(','):
        try:
            templist.append(float(number))
        except ValueError:
            pass
    #If we got a list we sort, cut the two extremes then average.
    if len(templist) > 3:
        templist.sort()
        del templist[len(templist) - 1]
        del templist[0]
        return reduce(lambda x, y: x + y, templist) / len(templist)
    #No data
    if not templist:
        return 0
    #If we dont we just take the number
    return templist[0]

def checkip(keyname, clientip):
    """
    Separating this test because pylint is anal.
    """
    oldip = cache.get(keyname)
    if not oldip:
        print('ip cache update: {} will be {}'.format(keyname, clientip))
        cache.set(keyname, clientip, 60*60*24*400)
    elif oldip != clientip:
        return False
    return True

@app.after_request
def add_header(response):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "0"
    response.headers['Cache-Control'] = 'public, max-age=0'
    return response

@app.route("/")
def hello():
    """Opening page with minimal information."""
    return render_template(
        'index.html',
        ip=request.environ['REMOTE_ADDR'],
        time=str(int(time.time())))

@app.route("/robot/")
def robot():
    """Scripted diagrams page"""
    from definitions import drawparams
    unitsraw = cache.get('robot-units')
    nevsorraw = cache.get('robot-nevsor')
    if not unitsraw or not nevsorraw:
        print("/robot/ cache update needed")
    units = json.loads(unitsraw)
    nevsor = json.loads(nevsorraw)
    return render_template(
        'robot.html',
        units=units,
        nevsor=nevsor,
        drawp=sorted(
            drawparams.iteritems(),
            key=lambda (k, v): (v['timespan'], k)),
        time=str(int(time.time())))

@app.route("/records/")
def recors():
    """This is a typo."""
    records = cache.get('records-rec')
    timelist = cache.get('records-tl')
    if not records or not timelist:
        print("/records/ cache update")
        pconn = pgconnect()
        pcur = pconn.cursor()
        pcur.execute('select * from toplist order by sortby,type,name,what desc;')
        records = {}
        scope = ''
        timelist = {}
        for line in pcur:
            if scope != line[7]:
                scope = line[7]
                records[scope] = []
                timelist[line[8]] = scope
            records[scope].append(line)
        pconn.close()
        cache.set('records-rec', records, 60*60*24)
        cache.set('records-tl', timelist, 60*60*24)
    return render_template('records.html', r=records, tl=timelist)

@app.route("/maps/")
def maps():
    """Collecting and transforming data to draw the flatmap."""
    rooms = cache.get('maps-rooms')
    limit = cache.get('maps-limit')
    if not rooms or not limit:
        print("/maps/ cache update")
        pconn = pgconnect()
        pcur = pconn.cursor()
        pcur.execute('select * from rooms;')
        rooms = {}
        limit = {}
        limit['mw'] = 0
        limit['mh'] = 0
        for room in pcur:
            rooms[room[3]] = {}
            rooms[room[3]]['sx'] = room[0][0] + room[1][0]
            rooms[room[3]]['sy'] = room[0][1] + room[1][1]
            rooms[room[3]]['x'] = room[1][0] + 6
            rooms[room[3]]['y'] = room[1][1] + 6
            rooms[room[3]]['w'] = room[2][0]
            rooms[room[3]]['h'] = room[2][1]
            limit['mw'] = max(limit['mw'], room[1][0] + room[2][0])
            limit['mh'] = max(limit['mh'], room[1][1] + room[2][1])
        limit['mh'] = limit['mh'] + 12
        limit['mw'] = limit['mw'] + 12
        pconn.close()
        cache.set('maps-rooms', rooms, 60*60*24)
        cache.set('maps-limit', limit, 60*60*24)
    return render_template('map.html', r=rooms, mw=limit['mw'], mh=limit['mh'])

@app.route("/maps/latest/")
def latest():
    """Returning a JSON with the actual data for the map."""
    from colormaps import cm_plasma, cm_viridis
    data = cache.get('latest')
    if not data:
        print("/latest/ cache update")
        pconn = pgconnect()
        pcur = pconn.cursor()
        pcur.execute((
            'select distinct on (name,type) name,temp,type,insdate from log '
            'where ( type=\'temp\' or type=\'hum\' ) and insdate > '
            'date_trunc(\'hour\', NOW() - interval \'2 hour\') '
            'order by name,type,insdate desc;'))
        data = {}
        for item in pcur:
            if not item[0] in data:
                data[item[0]] = {}
                data[item[0]]['text'] = '<b>{}</b>'.format(item[0])
            if item[2] == 'temp':
                normv = int((item[1] + 30)*(255/75))
                data[item[0]][item[2]] = cm_plasma[normv]
                data[item[0]]['text'] = '{}<br><small>{:.1f}C</small>'.format(
                    data[item[0]]['text'],
                    item[1])
            if item[2] == 'hum':
                normv = int((item[1] + 2)*(255/104))
                data[item[0]][item[2]] = cm_viridis[normv]
                data[item[0]]['text'] = '{}<br><small>{:.0f}%</small>'.format(
                    data[item[0]]['text'],
                    item[1])
        pconn.close()
        cache.set('latest', data, 59)
    resp = jsonify(data)
    return resp

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8081)
