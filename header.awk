BEGIN {
    name="";
} {
    if (name!=$NF) {
        printf("\n\n%s\n",$NF); 
        name=$NF;
    }; 
    print $0;
}
