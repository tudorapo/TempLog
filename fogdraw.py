# -*- coding: utf-8 -*-

import pytz
import argparse
import time
import logging
logger = logging.getLogger('peewee')
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.INFO)

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as nu
from matplotlib.colors import LogNorm



#The imports are messed up. I wanted to hide the pw/user from the code, which made this horror. sry.
from definitions import *

def perf(do, st, ct, tx):
    now = time.time()
    if do:
        print('After {:10} {:6.2f} {:.2}'.format(tx, now-st, now-ct))
    return now

parser = argparse.ArgumentParser()
parser.add_argument('-t', '--type', 
        help='Type of sensor to draw')
parser.add_argument('-n', '--name', 
        help='name of sensor to draw')
parser.add_argument('-s', '--slice', 
        help='Timeslice to draw')
parser.add_argument('-p', '--performance', 
        help='Writes out iming info', action='store_true')
parser.add_argument('-d', '--debug', 
        help='Writes out SQL sent to the server', action='store_true')
args = parser.parse_args()

if args.debug:
    logger.setLevel(logging.DEBUG)

database.execute_sql('SET TIME ZONE \'CET\';')

starttime = time.time()
lasttime = starttime

units = {}
unitdata = Units.select()
for unit in unitdata:
    units[unit.type]=(unit.unit, unit.title)
filename = 'static/fog_test.png'

lasttime = perf(args.performance, starttime, lasttime, 'units')

data =  Log.select(
        Log.name, 
        Log.type, 
        Log.temp,
        Log.insdate.alias('timeslice')
    ).where(
        (Log.insdate >  (dt.datetime.today() - dt.timedelta(weeks=56))) & 
        (Log.name == 'street') &
        (Log.type == 'temp')
    ).order_by(
        Log.name,
        SQL('timeslice').desc()
    )
#Rearranging the results to vectors, as that makes plotting easier.
d2 = data.dicts()
values = map(lambda d: d['temp'], d2)
times = map(lambda d: mdates.date2num(d['timeslice']), d2)
fig = plt.figure(figsize=(16,9), dpi=100)
ax = fig.add_subplot(111)
ax.xaxis_date('Europe/Budapest')
ax.xaxis.set_major_locator(mdates.MonthLocator(bymonthday=1))
ax.xaxis.set_minor_locator(mdates.WeekdayLocator(mdates.MO))
ax.xaxis.set_major_formatter(mdates.DateFormatter("%Y/%b", tz = mytz))
ax.grid(which='major',color='lightgrey')
ax.set_axisbelow(True)
ax.set_xlim(min(times),max(times))
wtf = ax.hist2d(times,values,bins=[112,40],label='ejnye',cmap=plt.get_cmap('Greys'),norm=LogNorm())
fig.colorbar(mappable=wtf[3])
fig.tight_layout()
fig.savefig(filename)
lasttime = perf(args.performance,starttime,lasttime,'draw')
