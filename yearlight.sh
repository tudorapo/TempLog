load 'shared.gpl'

set timefmt '%Y-%m-%d %H'
set xdata time
set format x '%a'
set xlabel 'Days'
set ylabel 'Log(light)'
set xtics 86400
set xrange [starttime:]

plot '/tmp/weeklight.txt' using 1:($2 == 0 ? 0 : log($2)) w boxes lw 2 notitle
