#!/bin/bash

cd /opt/csaszar/templog

. venv/bin/activate

MYCMD=$1
shift

python ./"$MYCMD".py "$@"

